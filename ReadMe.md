Magic of Books is a Book store which deals with book. All the inventory of the store is been managed
manually. but now as store is expending it becoming difficult to manage it manually. Books store
wants to have a book management system with following features.

## Admin Stories: -

1. As an Admin, I can add a new book
2. As an Admin, I can delete a book
3. As an Admin, I can update a book
4. As an Admin, I can display all the books
5. As an Admin, I can see the total count of the books
6. As an Admin, I can see the all the books under Autobiography genre
7. As an Admin, I can arrange the book in the following order
- price low to high- price high to low
- best selling

## Instructions: -

1. Please use the week 2 Assignment solution as a boilerplate for week 3
2. Please refactor class book and add Genre,noOfCopiesSold and price
3. Please create proper tables in the database
4. Connect the Java code with the database
5. Use map to store books in key value pair where key is book Id and value is Book object
6. Use generic as and when possible to restrict the collection.
7. create MagicOfBooks class to define all the functionality related methods.
8. Use custom exceptions to share the proper error message to the user when the user uses
theapplication
9. Zero marks will be awarded if the code throws a compile time error. Partial marking will be
doneonly if the code has no compile time error
10. Use Unit test cases with DAO Classes.
11. Use proper packages and coding standards in the application
