package com.magicofbooks.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnect {

	private static Connection connection = null;

	private DatabaseConnect() {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/MagicOfBook";
			String user = "root";
			String password = "Nallala@9398";

			// create a connection to the database
			connection = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {
		if (connection == null) {
			new DatabaseConnect();
		}
		return connection;
	}
}
