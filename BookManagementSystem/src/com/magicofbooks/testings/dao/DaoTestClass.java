package com.magicofbooks.testings.dao;

import static org.junit.Assert.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.magicofbooks.dao.MagicOfBooks;
import com.magicofbooks.pojo.Admin;
import com.magicofbooks.pojo.Book;
import com.magicofbooks.pojo.User;

public class DaoTestClass {

	static MagicOfBooks magicOfBooks;
	Book book;
	Book book1;
	
	@BeforeClass
	public static void setUp() {
		magicOfBooks = new MagicOfBooks();
	}
	
	@Before
	public void setUpBookObject() {
		book = new Book(7, "Book7", "Description7", "Author7", "Biography", 12, 10000);
		book1 = new Book(3, "Book33", "Description33", "Author33", "Biography", 12, 10000);
		
	}
	
	@Test
	public void addBookInDatabaseTest() {
		int test = magicOfBooks.addBook(book);
		assertEquals(1, test);
	}

	@Test
	public void deleteBookInDatabaseTest() {
		int test = magicOfBooks.deleteBook(book);
		assertEquals(1, test);
	}
	
	@Test
	public void updateBookInDatabaseTest() {
		int test = magicOfBooks.updateBook(book1);
		assertEquals(1, test);
	}
	
	@Test
	public void countOfBooksInDatabaseTest() {
		int count = magicOfBooks.countOfBooks();
		assertEquals(7, count);
	}
	
	@Test
	public void adminLoginTest() {
		Admin user = new Admin();
		user.setUsername("admin");
		user.setPassword("admin");
		int result = magicOfBooks.adminsLogin(user);
		assertEquals(1, result);
	}
	
	@Test
	public void userLoginTest() {
		User user = new User();
		user.setUsername("user");
		user.setPassword("user");
		User resultObject = magicOfBooks.usersLogin(user);
		assertEquals(user, resultObject);
	}
	
	@AfterClass
	public static void tearUp() {
		magicOfBooks = null;
	}
	
}
