package com.magicofbooks.pojo;

import java.util.Map;

public class User {
	
	private int userid;
	private String username;
	private String password;
	private String emailId;
	
	private Map<Integer,Book> newBooks;
	private Map<Integer,Book> favoriteBooks;
	private Map<Integer,Book> completedBooks;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public Map<Integer, Book> getNewBooks() {
		return newBooks;
	}
	public void setNewBooks(Map<Integer, Book> newBooks) {
		this.newBooks = newBooks;
	}
	
	
	public Map<Integer, Book> getFavoriteBooks() {
		return favoriteBooks;
	}
	public void setFavoriteBooks(Map<Integer, Book> favoriteBooks) {
		this.favoriteBooks = favoriteBooks;
	}
	
	
	public Map<Integer, Book> getCompletedBooks() {
		return completedBooks;
	}
	public void setCompletedBooks(Map<Integer, Book> completedBooks) {
		this.completedBooks = completedBooks;
	}
	
	
}
