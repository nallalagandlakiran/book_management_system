package com.magicofbooks.driver;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;


public class App{
	
	public static Logger logger;
	
	
	public void appStartUp() throws InterruptedException {
		Scanner scan = new Scanner(System.in);
		AdminOperationClass adminOperationObject;
		UserOperationClass userOperationObject;
		while(true) {
			System.out.println("------------------ Welcome to Magic of books -----------------");
			System.out.println("1. Admin login page ");
			System.out.println("2. User login page");
			System.out.println("0. Exit");
			int choice = scan.nextInt();
			switch(choice) {
				
			case 1:
				adminOperationObject = new AdminOperationClass();
				adminOperationObject.setLogger(logger);
				adminOperationObject.start();
				adminOperationObject.join();
				
				break;
			case 2:
				userOperationObject = new UserOperationClass();
				userOperationObject.setLogger(logger);
				userOperationObject.start();
				userOperationObject.join();
				break;
			case 0:
				scan.close();
				System.out.print("Thanks for using the application !!! Visit again");
				logger.info("App is closed by the user");
				System.exit(0);
				
			default:
				System.out.println("Invalid Choice!!! Enter choice between (0,1,2)");
			}
		}
		
	}
	public static void main(String[] args) {
		/*
		 * This App class extends thread, So this class is thread
		 * So when ever user logs in the application a thread is created and started
		 */
		FileHandler logsHandler = null;
		
		String regex = "/";
		String replace = "\\";
		
		/*
		 Getting the information of the current directory to store the logs in the same folder of this APP running
		 */
		String currentDirectory = System.getProperty("user.dir");	
		
		currentDirectory = currentDirectory.replaceAll(regex, replace);
		
		currentDirectory = currentDirectory + "\\MagicOfBooksLogFiles\\";
		
		File creatingDirectory = new File(currentDirectory);
		
		/*
		 * if the MagicOfBooksLogFiles directory is not in  Current working directory
		 * 		then Creating the directory in the same folder
		 */
		if(!creatingDirectory.exists()) {
			boolean status = creatingDirectory.mkdir();
			if(!status) {
				System.out.println("Error in creating directory for logs");
			}
		}
		
		try {
			logsHandler = new FileHandler(currentDirectory + "MOB_logs", true);
			logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.addHandler(logsHandler);
			logger.info("Logging started");
			App app = new App();
			app.appStartUp();
			
		} catch (SecurityException | IOException e) {
			System.out.println(e.getMessage());
			System.out.println("Try restarting the application !!!");
			
		} catch (InterruptedException e) {
			System.out.println("Failed to start the application !!! Try restart the application");
			System.out.println(e.getMessage());
		}
	}
}
