package com.magicofbooks.driver;

import java.util.Scanner;
import java.util.logging.Logger;

import com.magicofbooks.services.AdminService;

public class AdminOperationClass extends Thread {

	private AdminService adminService;
	private Scanner scan;
	private Logger logger;

	public AdminOperationClass() {
		adminService = new AdminService();
		scan = new Scanner(System.in);
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void run() {
		while (true) {
			if (adminService.login() == 1) {
				logger.info("admin logged into his account");
				System.out.println("Login successfull");
				if (adminDashboard() == 0) {
					System.out.println("Logged out successfully");
					logger.info("Admin logged out");
					break;
				}
			} else {
				System.out.println("Login failed !!! Invalid username OR password");
				System.out.println("Enter y to try again OR n to Start up");
				if (scan.next().charAt(0) != 'y') {
					break;
				}
			}
		}
	}

	private int adminDashboard() {
		int choice = 0;
		while (true) {
			adminMenu();
			choice = scan.nextInt();
			switch (choice) {
			case 1:
				adminService.addBookService();
				logger.info("Admin try to add new book in the store");
				break;

			case 2:
				adminService.delteBookServie();
				logger.info("Admin try to delete book in the store");
				break;

			case 3:
				adminService.updateBookService();
				logger.info("Admin try to update book details in the store");
				break;

			case 4:
				adminService.displayAllBooksService();
				logger.info("Admin viewed all the books in the store");
				break;

			case 5:
				adminService.totalBooksCount();
				logger.info("Admin counted the books in the store");
				break;

			case 6:
				adminService.displayAllBooksAutobiographyService();
				break;

			case 7:
				filterMenu();
				logger.info("Admin filtered the books in the store");
				break;

			case 0:
				return 0;
			default:
				logger.info("Admin entered an invalid operation");
				System.out.println("Wrong Choice!!! Enter choice between (");

			}
		}
	}

	private void adminMenu() {
		System.out.println("--------------- ADMIN MENU ----------------");
		System.out.println("1. Insert book into store");
		System.out.println("2. Delete book in store");
		System.out.println("3. Update book details in store");
		System.out.println("4. Display all books in store");
		System.out.println("5. Count of books in store");
		System.out.println("6. Display all books in Autobiography genre");
		System.out.println("7. Arrange the books in store");
		System.out.println("0. Log out");
		System.out.println("Enter your choice");
	}

	private void filterMenu() {
		int choice = 1;
		while (true) {
			System.out.println("-------------- arrangement order --------------");
			System.out.println("1. Low to High price");
			System.out.println("2. High to Low price");
			System.out.println("3. Best selling ");
			System.out.println("Enter your choice ");
			choice = scan.nextInt();
			switch (choice) {
			case 1:
				adminService.displayBooksLowToHighPrice();
				break;

			case 2:
				adminService.displayBooksHighToLowPrice();
				break;

			case 3:
				adminService.displayBooksBestSellingService();
				break;

			default:
				System.out.println("Wrong choice!!! Enter choice between(1..3)");
				break;
			}
			if(choice>0 && choice<4) {
				break;
			}
		}
	}

}
