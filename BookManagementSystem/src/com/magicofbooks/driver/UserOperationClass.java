package com.magicofbooks.driver;

import java.util.Scanner;
import java.util.logging.Logger;

import com.magicofbooks.services.UserService;

public class UserOperationClass extends Thread {

	private Scanner scan;
	private UserService userService;
	private Logger logger;

	public UserOperationClass() {
		scan = new Scanner(System.in);
		userService = new UserService();
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void run() {
		while (true) {
			if (userService.login() == 1) {
				logger.info("User logged into his account successfully");
				if (userDashboard() == 0) {
					System.out.println("Logged out successfully");
					logger.info("User logged out from his account successfully");
					break;
				}
			} else {
				System.out.println("Login failed !!! Invalid username OR password");
				System.out.println("Enter y to try again OR n to Start up");
				if (scan.next().charAt(0) != 'y') {
					break;
				}
			}
		}
	}

	private int userDashboard() {
		while (true) {
			userMenu();
			int choice = scan.nextInt();
			switch (choice) {
			case 1:
				userService.displayAllCatogeryBooks();
				logger.info("User viewed all books in his collection");
				break;

			case 2:
				userService.selectBookService();
				logger.info("User selected a book from his collection");
				break;

			case 3:
				logger.info("User viewed complete book details in his collection");
				userService.getCompleteBookDetailsService();
				break;

			case 0:
				return 0;

			default:
				break;
			}
		}
	}

	private void userMenu() {
		System.out.println("------------------User Menu-----------------");
		System.out.println("1. Display all the books collection");
		System.out.println("2. Select book from the collection");
		System.out.println("3. Display complete book details of a book");
		System.out.println("0. Log out");
		System.out.println("Enter your choice");
	}

}
