package com.magicofbooks.exception;

public class InvalidIDException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidIDException() {
		super("Exception Invalid book ID!!! book cannot have negative id");
	}
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
}
