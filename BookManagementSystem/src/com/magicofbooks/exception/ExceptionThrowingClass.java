package com.magicofbooks.exception;

public class ExceptionThrowingClass {

	public static void checkID(int id) throws InvalidIDException {
		
		if(id < 0) {
			throw new InvalidIDException();
		}
	}
}
