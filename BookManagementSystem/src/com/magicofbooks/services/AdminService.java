package com.magicofbooks.services;

import java.util.Map;
import java.util.Scanner;

import com.magicofbooks.dao.MagicOfBooks;
import com.magicofbooks.exception.ExceptionThrowingClass;
import com.magicofbooks.exception.InvalidIDException;
import com.magicofbooks.pojo.Admin;
import com.magicofbooks.pojo.Book;

public class AdminService {

	Scanner scan;
	MagicOfBooks magicOfBooks;
	Book book;
	
	public AdminService() {
		scan = new Scanner(System.in);
		magicOfBooks = new MagicOfBooks();
		book = new Book();
	}
	
	public int login() {
		System.out.println("--------------------- ADMIN MENU --------------------");
		System.out.println("Enter username");
		String username = scan.next();
		System.out.println("Enter password");
		String password = scan.next();
		Admin admin = new Admin();
		admin.setUsername(username);
		admin.setPassword(password);
		if(magicOfBooks.adminsLogin(admin) > 0) {
			return 1;
		}
		else {
			return 0;
		}
	}
	
	public void addBookService() {
		System.out.println("Enter bookId");
		int bookid = scan.nextInt();
		try {
			ExceptionThrowingClass.checkID(bookid);
			System.out.println("Enter book name");
			String bookName = scan.next();
			System.out.println("Enter book description");
			String description = scan.next();
			System.out.println("Enter author name");
			String author =scan.next();
			System.out.println("Enter book genre");
			String genre = scan.next();
			System.out.println("Enter number of copies sold");
			int noOfCopiesSold = scan.nextInt();
			System.out.println("Enter prcie of the book");
			Double price = scan.nextDouble();
			book = new Book(bookid, bookName, description, author, genre, noOfCopiesSold, price);
			if(magicOfBooks.addBook(book) > 0) {
				System.out.println("Book added successfully");
			}
			else {
				System.out.println("Book failed to add !! Try entering different bookid");
			}
		} catch (InvalidIDException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void updateBookService() {
		System.out.println("Enter book id ");
		int bookid = scan.nextInt();
		try {
			ExceptionThrowingClass.checkID(bookid);
			Book book = new Book();
			book.setBookId(bookid);
			book = magicOfBooks.searchBook(book);
			System.out.println("Enter book name, old book name is '" + book.getBookName() + "'");
			String bookName = scan.next();
			System.out.println("Enter book description, old book description is '" + book.getBookDescription() + "'" );
			String description = scan.next();
			System.out.println("Enter author name, old author name is '" + book.getAuthorName() + "'");
			String author =scan.next();
			System.out.println("Enter book genre, old genre is '" + book.getGenere() + "'");
			String genre = scan.next();
			System.out.println("Enter number of copies sold, ");
			int noOfCopiesSold = scan.nextInt();
			System.out.println("Enter prcie of the book");
			double price = scan.nextDouble();
			book = new Book();
			book.setBookId(bookid);
			book.setBookDescription(description);
			book.setGenere(genre);
			book.setNoOfCopiedSold(noOfCopiesSold);
			book.setPrice(price);
			book.setAuthorName(author);
			book.setBookName(bookName);
			if(magicOfBooks.updateBook(book) > 0) {
				System.out.println("Book details updated successfully successfully");
			}
			else {
				System.out.println("Book details failed to update !! Try entering different bookid");
			}
		} catch (InvalidIDException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public void delteBookServie() {
		System.out.println("Enter book id ");
		int bookId = scan.nextInt();
		try {
			ExceptionThrowingClass.checkID(bookId);
			book = new Book();
			book.setBookId(bookId);
			if(magicOfBooks.deleteBook(book) > 0) {
				System.out.println("Book deleted successfully");
			}
			else {
				System.out.println("Book with this id not found ");
			}
		} catch (InvalidIDException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void displayAllBooksService() {
		Map<Integer, Book> booksList;
		booksList = magicOfBooks.fetchAllBooks();
		System.out.printf("|%-10s|%-20s|%-25s|%-20s|%-20s|%-20s|%-10s|\n","bookId","bookName","bookDescription", "authorName","genere","noOfCopiedSold","price");
		for(Integer bookIds : booksList.keySet()) {
			System.out.println(booksList.get(bookIds));
		}
	}
	
	public void totalBooksCount() {
		int numberOfBooks = magicOfBooks.countOfBooks();
		System.out.println("Total number of books - " + numberOfBooks);
	}
	
	public void displayAllBooksAutobiographyService() {
		Map<Integer, Book> booksList;
		Book book = new Book();
		book.setGenere("autobiography");
		booksList = magicOfBooks.fetchAllAutobiogarphies(book);
		System.out.printf("|%-10s|%-20s|%-25s|%-20s|%-20s|%-20s|%-10s|\n","bookId","bookName","bookDescription", "authorName","genere","noOfCopiedSold","price");
		for(Integer bookIds : booksList.keySet()) {
			System.out.println(booksList.get(bookIds));
		}
	}
	
	public void displayBooksHighToLowPrice() {
		Map<Integer, Book> booksList;
		booksList = magicOfBooks.fetchAllBooksHighToLowPrice();
		System.out.printf("|%-10s|%-20s|%-25s|%-20s|%-20s|%-20s|%-10s|\n","bookId","bookName","bookDescription", "authorName","genere","noOfCopiedSold","price");
		for(Integer bookIds : booksList.keySet()) {
			System.out.println(booksList.get(bookIds));
		}
	}
	
	public void displayBooksLowToHighPrice() {
		Map<Integer, Book> booksList;
		booksList = magicOfBooks.fetchAllBooksLowToHighPrice();
		System.out.printf("|%-10s|%-20s|%-25s|%-20s|%-20s|%-20s|%-10s|\n","bookId","bookName","bookDescription", "authorName","genere","noOfCopiedSold","price");
		for(Integer bookIds : booksList.keySet()) {
			System.out.println(booksList.get(bookIds));
		}
	}
	
	public void displayBooksBestSellingService() {
		Map<Integer, Book> booksList;
		booksList = magicOfBooks.BestSellingBooksAsscendingOrder();
		System.out.printf("|%-10s|%-20s|%-25s|%-20s|%-20s|%-20s|%-10s|\n","bookId","bookName","bookDescription", "authorName","genere","noOfCopiedSold","price");
		for(Integer bookIds : booksList.keySet()) {
			System.out.println(booksList.get(bookIds));
		}
	}
}
