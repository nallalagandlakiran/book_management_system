package com.magicofbooks.services;

import java.util.Map;
import java.util.Scanner;

import com.magicofbooks.dao.MagicOfBooks;
import com.magicofbooks.exception.ExceptionThrowingClass;
import com.magicofbooks.exception.InvalidIDException;
import com.magicofbooks.pojo.Book;
import com.magicofbooks.pojo.User;

public class UserService {

	Scanner scan;
	MagicOfBooks magicOfBooks;
	User userObject;
	public UserService() {
		scan = new Scanner(System.in);
		magicOfBooks = new MagicOfBooks();
	}
	
	public int login() {
		System.out.println("--------------- USER MENU --------------");
		System.out.println("Enter username");
		String username = scan.next();
		System.out.println("Enter password");
		String password = scan.next();
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		userObject = magicOfBooks.usersLogin(user);
		if(userObject != null) {
			userObject.setFavoriteBooks(magicOfBooks.setAllFavoriteBooks(user));
			userObject.setCompletedBooks(magicOfBooks.setAllCompletedBooks(user));
			userObject.setNewBooks(magicOfBooks.setAllNewBooks(user));
			return 1;
		}
		return 0;
	}
	
	public void displayAllCatogeryBooks() {
		System.out.printf("%-10s %-20s %-10s\n", "Book Id", "Book Name", "Book Price");
		Map<Integer, Book> books = userObject.getNewBooks();
		for(Integer bookId : books.keySet()) {
			Book book = books.get(bookId);
			System.out.printf("%-10s %-20s %-10s\n",""+book.getBookId(), book.getBookName(),book.getPrice());
		}
		books = userObject.getFavoriteBooks();
		for(Integer bookId : books.keySet()) {
			Book book = books.get(bookId);
			System.out.printf("%-10s %-20s %-10s\n",""+book.getBookId(), book.getBookName(),book.getPrice());
		}
		books = userObject.getCompletedBooks();
		for(Integer bookId : books.keySet()) {
			Book book = books.get(bookId);
			System.out.printf("%-10s %-20s %-10s\n",""+book.getBookId(), book.getBookName(),book.getPrice());
		}
	}
	
	public void selectBookService() {
		System.out.println("Enter book id to select");
		int id = scan.nextInt();
		Map<Integer, Book> books = userObject.getNewBooks();
		for(Integer bookId : books.keySet()) {
			Book book = books.get(bookId);
			if(book.getBookId() == id) {
				System.out.println("The book '" + book.getBookName() + "' is selected of price " + book.getPrice() + "rs.");
				return;
			}
		}
		books = userObject.getFavoriteBooks();
		for(Integer bookId : books.keySet()) {
			Book book = books.get(bookId);
			if(book.getBookId() == id) {
				System.out.println("The book '" + book.getBookName() + "' is selected of price " + book.getPrice() + "rs.");
				return;
			}
		}
		books = userObject.getCompletedBooks();
		for(Integer bookId : books.keySet()) {
			Book book = books.get(bookId);
			if(book.getBookId() == id) {
				System.out.println("The book '" + book.getBookName() + "' is selected of price " + book.getPrice() + "rs.");
				return;
			}
		}
		System.out.println("No Book found with this id in your collection !! Try different id");
	}
	
	public void getCompleteBookDetailsService() {
		System.out.println("Enter book id to select");
		int id = scan.nextInt();
		try {
			ExceptionThrowingClass.checkID(id);
			Map<Integer, Book> books = userObject.getNewBooks();
			for(Integer bookId : books.keySet()) {
				Book book = books.get(bookId);
				if(book.getBookId() == id) {
					System.out.println("Details");
					System.out.println("[ Book Name		: " + book.getBookName() 
								    	+ "\nBook decription		: " + book.getBookDescription()
										+ "\nauthor name		: " + book.getAuthorName()
										+ "\nPrice			: " + book.getPrice() + "]");
					return;
				}
			}
			books = userObject.getFavoriteBooks();
			for(Integer bookId : books.keySet()) {
				Book book = books.get(bookId);
				if(book.getBookId() == id) {
					System.out.println("Details");
					System.out.println("[ Book Name		: " + book.getBookName() 
			    					+ "\nBook decription		: " + book.getBookDescription()
			    					+ "\nauthor name		: " + book.getAuthorName()
			    					+ "\nPrice			: " + book.getPrice() + "]");
					return;
				}
			}
			books = userObject.getCompletedBooks();
			for(Integer bookId : books.keySet()) {
				Book book = books.get(bookId);
				if(book.getBookId() == id) {
					System.out.println("Details");
					System.out.println("[ Book Name		: " + book.getBookName() 
			    					+ "\nBook decription		: " + book.getBookDescription()
			    					+ "\nauthor name		: " + book.getAuthorName()
			    					+ "\nPrice			: " + book.getPrice() + "]");
					return;
				}
			}
			System.out.println("No Book found with this id in your collection !! Try different id");
		} catch (InvalidIDException e) {
			System.out.println(e.getMessage());
		}
	}
	
}
