package com.magicofbooks.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magicofbooks.connection.DatabaseConnect;
import com.magicofbooks.pojo.Admin;
import com.magicofbooks.pojo.Book;
import com.magicofbooks.pojo.User;

public class MagicOfBooks {

	private PreparedStatement statement;
	private Connection databaseConnection;

	public MagicOfBooks() {
		databaseConnection = DatabaseConnect.getConnection();
	}

	public int addBook(Book bookObject) {

		try {
			statement = databaseConnection.prepareStatement("INSERT INTO books VALUES (?,?,?,?,?,?,?)");
			statement.setInt(1, bookObject.getBookId());
			statement.setString(2, bookObject.getBookName());
			statement.setString(3, bookObject.getBookDescription());
			statement.setString(4, bookObject.getAuthorName());
			statement.setString(5, bookObject.getGenere());
			statement.setInt(6, bookObject.getNoOfCopiedSold());
			statement.setDouble(7, bookObject.getPrice());
			int result = statement.executeUpdate();
			return result;
		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		return 0;
	}

	public int updateBook(Book bookObject) {
		try {
			statement = databaseConnection.prepareStatement(
					"UPDATE  books SET bookname = ?, description = ?, author = ?, genre = ?, copiessold = ?, price = ? WHERE bookid = ?");
			statement.setString(1, bookObject.getBookName());
			statement.setString(2, bookObject.getBookDescription());
			statement.setString(3, bookObject.getAuthorName());
			statement.setString(4, bookObject.getGenere());
			statement.setInt(5, bookObject.getNoOfCopiedSold());
			statement.setDouble(6, bookObject.getPrice());
			statement.setInt(7, bookObject.getBookId());
			int result = statement.executeUpdate();
			return result;
		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		return 0;
	}

	private int deleteBookIdInChildTables(int id) {
		try {
			int result1 = 0;
			int result2 = 0;
			int result3 = 0;
			statement = databaseConnection.prepareStatement("DELETE FROM favouritebooks where booksid = ?");
			statement.setInt(1, id);
			result1 = statement.executeUpdate();
			statement = databaseConnection.prepareStatement("DELETE FROM completedbooks where booksid = ?");
			statement.setInt(1, id);
			result2 = statement.executeUpdate();
			statement = databaseConnection.prepareStatement("DELETE FROM newbooks where booksid = ?");
			statement.setInt(1, id);
			result3 = statement.executeUpdate();
			if (result1 == 1 || result2 == 1 || result3 == 1) {
				return 1;
			}

		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		return 0;
	}

	public int deleteBook(Book bookObject) {
		try {
			deleteBookIdInChildTables(bookObject.getBookId());
			statement = databaseConnection.prepareStatement("DELETE FROM books WHERE bookid = ?");
			statement.setInt(1, bookObject.getBookId());
			int result = statement.executeUpdate();
			return result;
		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		return 0;
	}

	public Map<Integer, Book> fetchAllBooks() {

		try {
			statement = databaseConnection.prepareStatement("SELECT * FROM books");
			ResultSet result = statement.executeQuery();
			Map<Integer, Book> booksList;
			booksList = generateListOfBooks(result);
			return booksList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public int countOfBooks() {

		try {
			statement = databaseConnection.prepareStatement("SELECT COUNT(*) FROM books");
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				return result.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return -1;
	}

	public Map<Integer, Book> fetchAllAutobiogarphies(Book bookObject) {
		try {
			statement = databaseConnection.prepareStatement("SELECT * FROM books WHERE genre = ?");
			statement.setString(1, bookObject.getGenere());
			ResultSet result = statement.executeQuery();
			Map<Integer, Book> booksList;
			booksList = generateListOfBooks(result);
			return booksList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Map<Integer, Book> fetchAllBooksHighToLowPrice() {
		try {
			statement = databaseConnection.prepareStatement("SELECT * FROM books ORDER BY price desc");
			ResultSet result = statement.executeQuery();
			Map<Integer, Book> booksList;
			booksList = generateListOfBooks(result);
			return booksList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Map<Integer, Book> fetchAllBooksLowToHighPrice() {
		try {
			statement = databaseConnection.prepareStatement("SELECT * FROM books ORDER BY price asc");
			ResultSet result = statement.executeQuery();
			Map<Integer, Book> booksList;
			booksList = generateListOfBooks(result);
			return booksList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Map<Integer, Book> BestSellingBooksAsscendingOrder() {
		try {
			statement = databaseConnection.prepareStatement("SELECT * FROM books ORDER BY copiessold asc");
			ResultSet result = statement.executeQuery();
			Map<Integer, Book> booksList;
			booksList = generateListOfBooks(result);
			return booksList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	private Map<Integer, Book> generateListOfBooks(ResultSet result) throws SQLException {
		Map<Integer, Book> booksList = new LinkedHashMap<Integer, Book>();
		while (result.next()) {
			Book book = new Book();
			book.setBookId(result.getInt(1));
			book.setBookName(result.getString(2));
			book.setBookDescription(result.getString(3));
			book.setAuthorName(result.getString(4));
			book.setGenere(result.getString(5));
			book.setNoOfCopiedSold(result.getInt(6));
			book.setPrice(result.getDouble(7));
			booksList.put(book.getBookId(), book);
		}

		return booksList;

	}

	public Book searchBook(Book bookObject) {
		try {
			statement = databaseConnection.prepareStatement("SELECT * FROM books WHERE bookid = ?");
			statement.setInt(1, bookObject.getBookId());
			ResultSet result = statement.executeQuery();
			Book book = new Book();
			while (result.next()) {
				book.setBookId(result.getInt(1));
				book.setBookName(result.getString(2));
				book.setBookDescription(result.getString(3));
				book.setAuthorName(result.getString(4));
				book.setGenere(result.getString(5));
				book.setNoOfCopiedSold(result.getInt(6));
				book.setPrice(result.getDouble(7));
			}
			return book;

		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		return null;
	}

	public int adminsLogin(Admin admin) {

		try {
			statement = databaseConnection
					.prepareStatement("SELECT COUNT(*) FROM admins WHERE username = ? and password = ?");
			statement.setString(1, admin.getUsername());
			statement.setString(2, admin.getPassword());
			ResultSet result = statement.executeQuery();
			result.next();
			return result.getInt(1);
		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		return 0;
	}

	public User usersLogin(User user) {

		try {
			statement = databaseConnection.prepareStatement("SELECT * FROM users WHERE username = ? and password = ?");
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getPassword());
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				user.setUserid(result.getInt(1));
				user.setEmailId(result.getString(4));
				return user;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Map<Integer, Book> setAllFavoriteBooks(User user) {
		try {
			statement = databaseConnection.prepareStatement(
					"SELECT * FROM books WHERE bookid in " + "(SELECT booksid FROM favouritebooks WHERE usersid = ?)");
			statement.setInt(1, user.getUserid());
			ResultSet result = statement.executeQuery();
			Map<Integer, Book> booksList = generateListOfBooks(result);
			return booksList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Map<Integer, Book> setAllCompletedBooks(User user) {
		try {
			statement = databaseConnection.prepareStatement(
					"SELECT * FROM books WHERE bookid in " + "(SELECT booksid FROM completedbooks WHERE usersid = ?)");
			statement.setInt(1, user.getUserid());
			ResultSet result = statement.executeQuery();
			Map<Integer, Book> booksList = generateListOfBooks(result);
			return booksList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Map<Integer, Book> setAllNewBooks(User user) {
		try {
			statement = databaseConnection.prepareStatement(
					"SELECT * FROM books WHERE bookid in " + "(SELECT booksid FROM newbooks WHERE usersid = ?)");
			statement.setInt(1, user.getUserid());
			ResultSet result = statement.executeQuery();
			Map<Integer, Book> booksList = generateListOfBooks(result);
			return booksList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}
