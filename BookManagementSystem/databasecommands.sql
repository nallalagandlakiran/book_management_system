create database bookstore;

use bookstore;

create table Books(
    bookid int primary key,
    bookname varchar(50),
    description varchar(100),
    author varchar(50),
    genre varchar(50),
    copiessold int,
    price double 
    );

create table admins(
    adminid int primary key,
    username varchar(50),
    password varchar(50),
    email varchar(50) 
    );

create table users(
    userid int primary key,
    username varchar(50),
    password varchar(50),
    email varchar(50) 
    );

create table newbooks(
    usersid int,
    booksid int,
    primary key (usersid, booksid)
);

create table completedbooks(
    usersid int,
    booksid int,
    primary key (usersid, booksid)
);

create table favouritebooks(
    usersid int,
    booksid int,
    primary key (usersid, booksid)
);


insert into books values(1, 'book1', 'description1', 'author1','autobiography', 10, 10000);
insert into books values(2, 'book2', 'description2', 'author2','autobiography', 20, 20000);
insert into books values(3, 'book3', 'description3', 'author3','autobiography', 30, 30000);
insert into books values(4, 'book4', 'description4', 'author4','autobiography', 40, 40000);
insert into books values(5, 'book5', 'description5', 'author5', 'autobiography',50, 50000);
insert into books values(6, 'book6', 'description6', 'author6','autobiography', 60, 60000);

insert into users values(101, 'user', 'user', 'user@mob.com');

insert into admins values(1001, 'admin', 'admin', 'admin@mob.com');

insert into favouritebooks values(101, 1);
insert into favouritebooks values(101, 2);

insert into completedbooks values(101, 3);
insert into completedbooks values(101, 4);

insert into newbooks values(101, 5);
insert into newbooks values(101, 6);


